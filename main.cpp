#include <iostream>
using namespace std;

class Animal 
{   
    public:
        Animal() {}
        virtual void Voice()
        {
            cout << "String\n";
        }
};

class Cat : public Animal
{
    public:
        Cat() {}
        virtual void Voice()
        {
            cout << "Meow!\n";
        }
};

class Dog : public Animal
{
    public:
        Dog() {}
        virtual void Voice()
        {
            cout << "Woof!\n";
        }
};

class Parrot : public Animal
{
    public:
        Parrot() {}
        virtual void Voice()
        {
            cout << "Bruh!\n";
        }
};

int main()
{
    Animal **animal = new Animal*[3];
    animal[0] = new Cat();
    animal[1] = new Dog();
    animal[2] = new Parrot();
    for (int i = 0; i < 3; i++)
    {
        animal[i]->Voice();
    }
}